<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-menus?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// M
	'menus_description' => 'Quando non utilizzi il plugin {{Menù}}, devi definire tutti i tuoi menu nei modelli, il che significa che gli amministratori del sito non hanno il controllo diretto su di esso e che quando vogliono cambiare qualcosa, devono chiedere al responsabile dei template. Inoltre, quando si desiderano collegamenti statici (un collegamento a un articolo specifico, o ad una pagina specifica, o ad un sito esterno) devono essere scritti nel menu template.

L’obiettivo del plugin {{Menù}} è quindi quello di semplificare la creazione di menu utilizzando un’interfaccia user-friendly, direttamente nell’area riservata.
{{Attenzione!}} Questo plugin non si occupa di come verranno visualizzati i menu. Ti permette di crearli facilmente e generare il codice HTML.',
	'menus_nom' => 'Menù',
	'menus_slogan' => 'Crea i tuoi menù personalizzati.',
	'menus_titre' => 'Menù',
];

# Changelog

## Unreleased

### Fixed

 - #33 Annonce de l'utilisation du plugin YAML pour l'export/import de la configuration du menu

## 3.0.1 - 2023-12-4

### Fixed

- #29 Réafficher correctement les entrées des menus lorsqu'on édite
